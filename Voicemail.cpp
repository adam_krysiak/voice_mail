/*
 * Voicemail.cpp
 *
 *  Created on: 21 mar 2016
 *      Author: adam
 */

#include "Voicemail.h"

#include <boost/regex.hpp>
#include <fstream>
#include <string>
#include <iostream>
#include <future>

bool Voice_mail::exit = false;

Voice_mail::Voice_mail() {

	MakeAnalize();

}

Voice_mail::~Voice_mail() {
	// TODO Auto-generated destructor stub
}


/*
 * function which frist compares connected MAC's with known ones, and asks if update
 * @return  returns count of connected Devices
 */
int Voice_mail::AnalyzeConnectedDevices() {

	CompareConnectedAndKnown();
	return this->connectedDevices.size();
}

void Voice_mail::CompareConnectedAndKnown() {
	for (auto &device : this->connectedDevices) {
		if (this->knownDevices.count(device.first))
		{
			this->connectedDevices[device.first].setName(this->knownDevices[device.first].getName());
			this->connectedDevices[device.first].setMac(this->knownDevices[device.first].getMac());

		}
	}

}

int Voice_mail::GetCounter() {
	return counter;
}

int Voice_mail::ReadFromDataBase() {
	fstream in("data.txt", ios::in);
	int count = 0;
	if (!in.fail()) {
		boost::regex reg("-m (.*) -o (.*)");
		string line = "";
		while (getline(in, line)) {
			boost::smatch score;
			if (boost::regex_search(line, score, reg)) {
				this->knownDevices[score[1]].setName(score[2]);
				this->knownDevices[score[1]].setMac(score[1]);
				count++;
			}

		}
		in.close();
		return count;
	}
	return 0;

}
void Voice_mail::WriteToDataBase() {
	fstream out("data.txt", ios::out);

	for (auto &value : this->knownDevices)
		out << "-m " << value.first << " -o " << value.second.getName() << endl;

	out.close();

}

string Voice_mail::AskAndReturnMacOwner(string a) {				//here i will use synthezator in future

	cout << "czy chcesz dodać właściciela adresu: " << a << endl
			<< "[t] - tak | [n] - nie]" << endl;
	char c;
	for (;;) {
		cin >> c;
		switch (c) {
		case 't': {
			string owner;
			cout << "podaj nazwę: ";
			cin >> owner;
			this->knownDevices[a].setName(owner);
			this->knownDevices[a].setMac(a);
			return owner;
		}
		case 'n': {
			return "";
		}
		default: {
			cout << "nie ma takiej opcji, wybierz [t] - tak lub [n] - nie"
					<< endl;
		}
		}
	}
	return "";

}

void Voice_mail::MakeAnalize() {

	MAC_analyzer::MAC_analize();
		for (auto & mac : MAC_analyzer::getAdresses())
			this->connectedDevices[mac] = User("", mac);
			this->ReadFromDataBase();
		AnalyzeConnectedDevices();
		this->WriteToDataBase();
}

void Voice_mail::AddUnknownDevices() {
	for (auto &device : this->connectedDevices)
			if (device.second.getName() == "")
				device.second.setName(AskAndReturnMacOwner(device.first));
}

const map<string, User>& Voice_mail::getConnectedDevices() const {
	return connectedDevices;
}

const map<string, User>& Voice_mail::getKnownDevices() const {
	return knownDevices;
}
