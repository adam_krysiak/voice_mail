/*
 * User.h
 *
 *  Created on: 22 mar 2016
 *      Author: adam
 */

#ifndef USER_H_
#define USER_H_
#include <iostream>
#include <vector>
using namespace std;

class User {
public:
	User(string, string);
	User();
	virtual ~User();


	bool operator==(const User & usr);
	User operator=(const User & usr);
	const string& getMac() const;
	void setMac(const string& mac);
	const string& getName() const;
	void setName(const string& name);
	void AddMessage();
	int HowManyMessages();

private:
	string name;
	string MAC;
	vector<string>messages;

};

#endif /* USER_H_ */
