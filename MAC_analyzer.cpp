/*
 * MACanalyzer.cpp
 *
 *  Created on: 15 mar 2016
 *      Author: Adam Krysiak
 */

#include <boost/regex.hpp>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "MAC_analyzer.h"


/*
 * class lets you use send ARP signal. After sending requests if finds MAC's in answear and saves in MAC_analyzer::MAC_adresses
 */
using namespace std;

vector<string>MAC_analyzer::MAC_adresses;

MAC_analyzer::MAC_analyzer() {
}

int MAC_analyzer::MAC_analize() {


	system("./ARP_signal.sh");				//this script is sending arp signal and saves answear in arp_echo
	fstream in("arp_echo.txt", ios::in);
	string temp;
	boost::regex reg("..:..:..:..:..:..");
	int counter = 0;
	if (!in.fail()) {
		while (getline(in, temp)) {
			boost::smatch MAC;
			if(boost::regex_search(temp,MAC, reg)){
	//				cout<<string(MAC[0])<<endl;
					MAC_analyzer::MAC_adresses.push_back(string(MAC[0]));		//adding to MAC::adresses
				}
			counter++;
		}

	}
	return counter;

}

int MAC_analyzer::getCount() {
	return MAC_analyzer::MAC_adresses.size();
}

vector<string> MAC_analyzer::getAdresses() {
	return MAC_analyzer::MAC_adresses;
}

MAC_analyzer::~MAC_analyzer() {
}

