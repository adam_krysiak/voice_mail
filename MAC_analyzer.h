/*
 * MACanalyzer.h
 *
 *  Created on: 15 mar 2016
 *      Author: Adam Krysiak
 */

#ifndef MACANALYZER_H_
#define MACANALYZER_H_

#include <string>
#include <vector>

using namespace std;

class MAC_analyzer {
public:
	static int MAC_analize();
	static int getCount();
	static vector<string> getAdresses();
private:
	static vector<string> MAC_adresses;
	MAC_analyzer();
	virtual ~MAC_analyzer();

};

#endif /* MACANALYZER_H_ */
