/*
 * Voicemail.h
 *
 *  Created on: 21 mar 2016
 *      Author: adam
 */

#ifndef VOICEMAIL_H_
#define VOICEMAIL_H_
#include "MAC_analyzer.h"
#include "User.h"
#include <iostream>
#include <map>
using namespace std;

class Voice_mail {
public:
	Voice_mail();
	virtual ~Voice_mail();
	int ReadFromDataBase();
	void WriteToDataBase();
	int AnalyzeConnectedDevices();
	void CompareConnectedAndKnown();
	int GetCounter();
	string AskAndReturnMacOwner(string);
	void MakeAnalize();
	void AddUnknownDevices();
	const map<string, User>& getConnectedDevices() const;
	const map<string, User>& getKnownDevices() const;

private:
	static bool exit;
	int counter;
	map<string, User> knownDevices;
	map<string, User> connectedDevices;

};

#endif /* VOICEMAIL_H_ */
