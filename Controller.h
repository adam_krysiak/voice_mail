/*
 * Controller.h
 *
 *  Created on: 7 kwi 2016
 *      Author: adam
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_
#include <iostream>
#include <memory>
#include <atomic>
#include "Voicemail.h"



class Controller {
public:
	Controller();
	virtual ~Controller();

	void startMenu();
	void startAnalization();
	void MakeAnalizeWithVoiceMail();
	std::unique_ptr<Voice_mail> voiceMailModel;
private:
	std::atomic <bool> end_analization;
};

#endif /* CONTROLLER_H_ */
